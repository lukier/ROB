function [sepplane fp fn] = perceptron(pclass, nclass)
% Computes separating plane (linear classifier) using
% perceptron method.
% pclass - 'positive' class (one row contains one sample)
% nclass - 'negative' class (one row contains one sample)
% Output:
% sepplane - row vector of separating plane coefficients
% fp - false positive count (i.e. number of misclassified samples of pclass)
% fn - false negative count (i.e. number of misclassified samples of nclass)

  sepplane = rand(1, columns(pclass) + 1) - 0.5;
  tset = [ ones(rows(pclass), 1) pclass; -ones(rows(nclass), 1) -nclass];

  i = 1;
  blad = zeros(1, 200);
  eta = 1;
  do 
    %Policz odleg³oc wzgledem p³aszczyzny
    res_class = tset*sepplane';
    %ktore blednie zaklasyfikowane
    not_in_class = res_class < 0;
   
    %staly wspolnczynnik nauki
    eta = 1;   
    
    %Obliczenie bledu
    blad(i) = sum(not_in_class);
    
    %Nowe parametry dla płaszczyzny rozdzielającej
    sepplane = sepplane + eta*sum(tset(not_in_class,:));
    
    if(i > 20)
      if(blad(i)-blad(i-15) <= 1)
        break;
       endif
    endif
    ++i;
  until i > 200;
  
  %Wyliczenie False Positive i False Negative
  fp = sum([ ones(rows(pclass), 1) pclass]*sepplane' < 0);
  fn = sum([ ones(rows(nclass), 1) nclass]*sepplane' >= 0);
  
