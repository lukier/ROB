function [pcaSet] = pcaTransform(tvec, mu, trmx)
% tvec - matrix containing vectors to be transformed
% mu - mean value of the training set
% trmx - pca transformation matrix
% pcaSet -  outpu set transforrmed to PCA  space

% this is too much for my Win32 configuration
%pcaSet = tvec - repmat(mu, size(tvec,1), 1);
%pcaSet = pcaSet * trmx;

% slower version requireing much less memory
pcaSet = zeros(rows(tvec), columns(trmx));
for i=1:size(tvec,1)
  pcaSet(i,:) = (tvec(i,:) - mu)*trmx;
end

