function ovosp = trainOVOensamble_g(tset, tlab, htrain, margin = 0.1)
% Działa tak samo jak trainOVOensamble
% Z tym że do nauki używa tylko 0.5*(wielkosc mniejszej grupy w parze) elemntów z każdej grupy pary

  labels = unique(tlab);
  
  % nchoosek produces all possible unique pairs of labels
  % that's exactly what we need for ovo classifier
  pairs = nchoosek(labels, 2);
  ovosp = zeros(rows(pairs), 2 + 1 + columns(tset));
  fp_agr = zeros(rows(pairs), 1);
  fn_agr = zeros(rows(pairs), 1);  
  size_agr = zeros(rows(pairs), 1);

  for i=1:rows(pairs)
	% store labels in the first two columns
    ovosp(i, 1:2) = pairs(i, :);
	
	% select samples of two digits from the training set
    posSamples = tset(tlab == pairs(i,1), :);
    negSamples = tset(tlab == pairs(i,2), :);

    N = 0;
    if(rows(posSamples) > rows(negSamples))
	N = rows(negSamples)*0.5
    else
    	N = rows(posSamples)*0.5
    end
    
    elPos = randperm(rows(posSamples))(1:N);
    elNeg = randperm(rows(negSamples))(1:N);
    N
    posSamples = posSamples(elPos, :);
    negSamples = negSamples(elNeg, :);

	% train 5 classifiers and select the best one
    [sp fp fn] = trainSelect(posSamples, negSamples, 5, htrain, margin);
	
    fp_agr(i) = fp;
    fn_agr(i) = fn;
    size_agr(i) = rows(posSamples) + rows(negSamples);
    % store the separating plane coefficients (this is our classifier)
	% in ovo matrix
    ovosp(i, 3:end) = sp; 
  end

  fp_mean = mean(fp_agr)
  fp_std = std(fp_agr)
  fn_mean = mean(fn_agr)
  fn_std = std(fn_agr)
  size_mean = mean(size_agr)

