function [najb_id] = class(ind_pun, zbior, cechy)  
  P = zbior(ind_pun, cechy);
  
  zbior_ind = zbior([1:(ind_pun-1) ind_pun+1:end], 1);
  zbior_war = zbior([1:(ind_pun-1) ind_pun+1:end], cechy);
  
  [~, najb_id] = min( sqrt( sumsq( ( zbior_war .- P)' ))); 
  
  najb_id = zbior_ind(najb_id);
endfunction