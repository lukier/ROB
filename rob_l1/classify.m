function [najb_id] = classify(P, zbior, cechy)  
  
  zbior_war = zbior(:, cechy);
  
  [~, najb_id] = min( sqrt( sumsq( ( zbior_war - repmat(P, rows(zbior_war), 1))' ))); 
 
  najb_id = zbior(najb_id,1);
endfunction