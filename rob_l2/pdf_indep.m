function pdf = pdf_indep(pts, para)
% Liczy funkcj� g�sto�ci prawdopodobie�stwa przy za�o�eniu, �e cechy s� niezale�ne
% pts zawiera punkty, dla kt�rych liczy si� f-cj� g�sto�ci (punkt = wiersz)
% para - struktura zawieraj�ca parametry:
%	para.mu - warto�ci �rednie cech (wiersz na klas�)
%	para.sig - odchylenia standardowe cech (wiersz na klas�)
% pdf - macierz g�sto�ci prawdopodobie�stwa
%	liczba wierszy = liczba pr�bek w pts
%	liczba kolumn = liczba klas

	% znam rozmiar wyniku, wi�c go alokuj�
	pdf = zeros(rows(pts), rows(para.mu));
	
	for cl = 1:rows(para.mu) % dla kazdej klasy
		for ft = 1:columns(para.mu) % dla kazdej cechy
			f_pdf(:,ft) = normpdf(pts(:,ft), para.mu(cl, ft), para.sig(cl, ft));
		end
		pdf(:,cl) = prod(f_pdf,2);
	end
end