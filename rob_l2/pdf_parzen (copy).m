function pdf = pdf_parzen(pts, para)
% Aproksymuje warto�� g�sto�ci prawdopodobie�stwa z wykorzystaniem okna Parzena
% pts zawiera punkty, dla kt�rych liczy si� f-cj� g�sto�ci (punkt = wiersz)
% para - struktura zawieraj�ca parametry:
%	para.samples - tablica kom�rek zawieraj�ca pr�bki z poszczeg�lnych klas
%	para.parzenw - szeroko�� okna Parzena
% pdf - macierz g�sto�ci prawdopodobie�stwa
%	liczba wierszy = liczba pr�bek w pts
%	liczba kolumn = liczba klas
	 
  n = rows(pts);
  cl_siz = rows(para.samples);
  u = 1;
  pdf = zeros(n, cl_siz);
  for i_pts=1:rows(pts)
    for cl=1:cl_siz
      samp = para.samples{cl};
      n = rows(samp);
      hn = para.parzenw/sqrt(n);
      mn_p = 0; 
      for i_samp=1:rows(samp)
	p_cech = 1;
	%for cech = 1:columns(samp)
	  u = (pts(i_pts,:)-samp(i_samp,:))/hn;
          p_cech  = prod(1/hn * e .^ (-(u.^2)/2) / sqrt(2*pi), 2);	  
	%end
	mn_p = mn_p + p_cech;
      end
      pdf(i_pts,cl)= mn_p / n;
    end

  end

 
  
 
