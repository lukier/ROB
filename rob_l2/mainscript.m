load train.txt
load test.txt

% pierwszy rzut oka na dane
size(train)
size(test)
labels = unique(train(:,1))
unique(test(:,1))

% pomy�lcie, co daje w wyniku poni�sze wyra�enie i dlaczego dzia�a
sum(train(:,1) == labels')

% pierwszym zadaniem po za�adowaniu danych jest sprawdzenie,
% czy w zbiorze ucz�cym nie ma pr�bek odstaj�cych
% do realizacji tego zadania przydadz� si� funkcje licz�ce
% proste statystyki: mean, median, std, 
% wy�wietlenie histogramu cech(y): hist
% spojrzenie na dwie cechy na raz: plot2features (dostarczona w pakiecie)

[mean(train); median(train)]
hist(train(:,1))
plot2features(train, 1, 2)

% do identyfikacji odstaj�cych pr�bek doskonale nadaj� si� wersje
% funkcji min i max z dwoma argumentami wyj�ciowymi

[mv midx] = max(train)

% poniewa� warto�ci minimalne czy maksymalne da si� wyznaczy� zawsze,
% dobrze zweryfikowa� ich odstawanie spogl�daj�c przynajmniej na s�siad�w
% podejrzanej pr�bki w zbiorze ucz�cym

% powiedzmy, �e podejrzana jest pr�bka 58 (kt�r� w istocie nale�y podejrzewa�?)
midx = 642
train(midx-1:midx+1, :)
% tu akurat wysz�y r�ne klasy, wi�c por�wnanie jest trudne...

% je�li nabra�em przekonania, �e pr�bka midx jest do usuni�cia, to:
size(train)
train(midx, :) = [];
size(train)

midx = 186
train(midx-1:midx+1, :)
% tu akurat wysz�y r�ne klasy, wi�c por�wnanie jest trudne...

% je�li nabra�em przekonania, �e pr�bka midx jest do usuni�cia, to:
size(train)
train(midx, :) = [];
size(train)

% procedur� szukania i usuwania warto�ci odstaj�cych trzeba powtarza� do skutku

% po usuni�ciu warto�ci odstaj�cych mo�na zaj�� si� wyborem DW�CH cech dla klasyfikacji
% w tym przypadku w zupe�no�ci wystarczy poogl�da� wykresy dw�ch cech i wybra� te, kt�re
% daj� w miar� dobrze odseparowane od siebie klasy
plot2features(train, 2, 4)



% to nie jest najros�dniejszy wyb�r, ale dla potrzeb prezentacji go u�ywam
train = train(:, [1 2 4 ]);
test = test(:, [1 2 4 ]);

% tutaj jawnie tworz� struktur� z parametrami dla klasyfikatora Bayesa 
% (po prawdzie, to dla funkcji licz�cej g�sto�� prawdobie�stwa) z za�o�eniem,
% �e cechy s� niezale�ne

% etykiety ju� by�y:
labels = unique(train(:,1));
pdfindep_para.labels = labels;

% za moment, w p�tli wyznacz� warto�ci �rednie i odchylenia standardowe
% dla wszystkich klas; wiem ju�, jakie b�d� wymiary stosownych macierzy,
% wi�c je alokuj� wype�niaj�c zerami
% obie macierze b�d� mie� wymiary (liczba_klas x liczba_cech)

pdfindep_para.mu = zeros(rows(labels), columns(train)-1);
pdfindep_para.sig = zeros(rows(labels), columns(train)-1);

% wreszcie licz� parametry dla pdf_indep
for i=1:rows(labels)
	% uwaga: w tym przypadku to sztuka dla sztuki (etykiety s� 1 2 3 4)
	% indeksowanie po�rednie zapewni, �e wszystko b�dzie dzia�a� dla dowolnych etykiet
	clabel = labels(i);
	pdfindep_para.mu(i, :) = mean(train(train(:,1)==clabel, 2:end));
	pdfindep_para.sig(i, :) = std(train(train(:,1)==clabel, 2:end));
end

% Struktura dla dwuwymiarowego rozk�adu normalnego musi zapami�ta� dla ka�dej klasy
% macierz kowariancji - wygodnie b�dzie u�y� macierzy tr�jwymiarowej

% parametry dla wielowymiarowego rozk�adu normalnego
pdfmulti_para.labels = labels;
pdfmulti_para.mu = zeros(rows(labels), columns(train)-1);
% UWAGA (bez �rednik�w, �eby by�o wida� wynik)
pdfmulti_para.sig = zeros(columns(train)-1, columns(train)-1, rows(labels))

% prosz� por�wna� indeksowanie
pdfmulti_para.sig(:,:,1)
pdfmulti_para.sig(1,:,:)

% funkcja cov liczy macierz kowariancji argumentu
for i=1:rows(labels)
	clabel = labels(i);
	pdfmulti_para.mu(i, :) = mean(train(train(:,1)==clabel, 2:end));
	pdfmulti_para.sig(:, :, i) = cov(train(train(:,1)==clabel, 2:end));
end

% parametry dla rozwi�zania z oknem Parzena
% tu w zasadzie jedna w�tpliwo�� zostaje: czy rozdziela� zbi�r ucz�cy na klasy 
% (mo�na pozby� si� etykiet), czy przekaza� go w ca�o�ci
% Je�li chcieliby�cie podzieli� zbi�r na klasy, natkniecie si� na problem: 
% klasy maj� r�ne liczby pr�bek, nie da si� zapisa� danych w macierzy 3d
% rozwi�zaniem jest tablica kom�rek (cell array), kt�ra mo�e zawiera� w ka�dej
% kom�rce inne rzeczy (w szczeg�lno�ci macierze o r�nej liczbie wierszy)

pdfparzen_para.labels = labels;

% wierszy tyle ile klas, 1 kolumna KOM�REK
pdfparzen_para.samples = cell(rows(labels),1)

% teraz wstawiamy pr�bki z poszczeg�lnych klas do kom�rek
for i=1:rows(labels)
	pdfparzen_para.samples{i} = train(train(:,1) == labels(i), 2:end);
%						  ^^^ istotne: dobieram si� do WN�TRZA kom�rki
end
pdfparzen_para.parzenw = 0.001;

% czym r�ni si� indeksowanie () i {} w przypadku tablic kom�rek
% dost�p do zawarto�ci kom�rki o indeksie 1
size(pdfparzen_para.samples{1})
% dost�p do kom�rki o indeksie 1
size(pdfparzen_para.samples(1))

% dla sprawdzenia funkcji licz�cych pdf zrobi� sobie ma�y zbi�r do test�w
% 4 pr�bki - ka�da z innej klasy
tst = train([1 21 41 61],2:end)

pdf_indep(tst, pdfindep_para)
%  1.5836e+011  0.0000e+000  2.1348e+010  5.7646e+010
%  1.0130e+011  0.0000e+000  6.4557e+010  1.9630e+010
%  7.2888e+010  8.2138e+016  1.2340e-005  4.6572e+010
%  7.7599e+010  0.0000e+000  1.1250e+012  1.7571e+010

pdf_multi(tst, pdfmulti_para)
%  3.6650e+011  0.0000e+000  3.6422e+009  1.8300e+011
%  5.7648e+009  0.0000e+000  2.1316e+010  1.2396e+009
%  7.0642e+007  1.0946e+017  5.0140e-019  2.1473e+007
%  2.5762e+011  0.0000e+000  4.8451e+011  4.1083e+010

pdf_parzen(tst, pdfparzen_para)
%  1.1978e+007  0.0000e+000  0.0000e+000  2.5243e+008
%  9.3703e+007  0.0000e+000  6.3710e+007  0.0000e+000
%  2.5945e-024  6.7958e+009  0.0000e+000  4.9371e+007
%  1.4951e+008  0.0000e+000  2.1525e+008  0.0000e+000

% funkcje g�sto�ci dzia�aj�, zatem mo�na po kolei wykona� nastepne punkty programu
base_ercf = zeros(1,3);
base_ercf(1) = mean(bayescls(test(:,2:end), @pdf_indep, pdfindep_para) != test(:,1));
base_ercf(2) = mean(bayescls(test(:,2:end), @pdf_multi, pdfmulti_para) != test(:,1));
base_ercf(3) = mean(bayescls(test(:,2:end), @pdf_parzen, pdfparzen_para) != test(:,1));
base_ercf

% do punktu 3. przyda si� funkcja reduce, kt�ra losuje z poszczeg�lnych klas 
% odpowiedni� cz�� pr�bek
% poniewa� jest zmieniany zbi�r ucz�cy, za ka�dym razem trzeba wyznaczy� parametry
% dla wszystkich funkcji pdf
% trzeba pami�ta� o powt�rzeniu eksperymentu 5 (lub wi�cej) razy
% w sprawozdaniu prosz� umie�ci� warto�� �redni� i odchylenie standardowe wsp. b��du 

ratio = [0.1 0.25 0.5 1.0];

n_all = rows(train);

for rat_num=1:0%columns(ratio)
  base_ercf = zeros(3,5);
  for i = 1:5
    red_train = train(randperm(n_all, ceil(n_all*ratio(rat_num))), 1:end);

    pdfindep_para = para_indep(red_train);

    pdfmulti_para = para_multi(red_train);

    pdfparzen_para = para_parzen(red_train,0.0001);
    
    
    base_ercf(1,i) += mean(bayescls(test(:,2:end), @pdf_indep, pdfindep_para) != test(:,1));
    base_ercf(2,i) += mean(bayescls(test(:,2:end), @pdf_multi, pdfmulti_para) != test(:,1));
    base_ercf(3,i) += mean(bayescls(test(:,2:end), @pdf_parzen, pdfparzen_para) != test(:,1));
  end
  rat_num 

  rat_mean=mean(base_ercf,2)
  rat_min=min(base_ercf,[],2)
  rat_max=max(base_ercf,[],2)
  rat_std=std(base_ercf,[],2)
end



% punkt 4. jest wykonywany znowu na pe�nym zbiorze danych

parzen_widths = [0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1, 5, 10];
parzen_res = zeros(1, columns(parzen_widths));

for parz_i = 1:0%columns(parzen_widths)
    pdfparzen_para = para_parzen(train, parzen_widths(parz_i));
    base = mean(bayescls(test(:,2:end), @pdf_parzen, pdfparzen_para) != test(:,1));
    parzen_res(1,parz_i) = base
end
save parzen_res.mat parzen_res

% a� si� prosi, �eby pr�cz tabelki, zamie�ci� w sprawozdaniu wykres
% ze wzgl�du na rozrzut szeroko�ci okna dobrze sprawdzi si� skala logarytmiczna na osi x
semilogx(parzen_widths, parzen_res)

% w punkcie 5. redukcja dotyczy ZBIORU TESTOWEGO
% dla ustalenia uwagi zostawiamy pe�ny zbi�r ucz�cy
apriori = [0.17 0.33 0.33 0.17];
parts = [0.5 1.0 1.0 0.5];

train_red=reduce(train,apriori);

pdfparzen_para = para_parzen(train_red,  0.001);
base_red = mean(bayescls(test(:,2:end), @pdf_parzen, pdfparzen_para) != test(:,1))


% w punkcie 6. du�e znaczenie ma normalizacja danych
% decyzj� mo�na podj�� patrz�c na odchylenia standardowe obu cech
nn_res = mean(leave_and_out(train, [2 3])' != train(:,1))


std(train(:,2:end))
norm_train = train;
norm_test = test;

for i=2:columns(train)
    min_c = min(train(:,i))
    ran_c = max(train(:,i))-min_c

    norm_train(:,i)=norm_train(:,i)-min_c;
    norm_train(:,i)=norm_train(:,i)*1/ran_c;
    norm_test(:,i)=norm_test(:,i)-min_c;
    norm_test(:,i)=norm_test(:,i)*1/ran_c;
end

std(norm_train(:,2:end))
min(norm_test(:,:))
max(norm_test(:,:))
nn_res_norm = mean(leave_and_out(norm_train, [2 3])' != norm_train(:,1))

pdfparzen_para = para_parzen(norm_train,  0.05);
base_norm = mean(bayescls(norm_test(:,2:end), @pdf_parzen, pdfparzen_para) != norm_test(:,1))


% pami�tajcie o tym, �e te same wsp�czynniki normalizacji nale�y
% u�y� do zbioru ucz�cego i zbioru testowego

