function rds = reduce(ds, parts)
% Funkcja redukcji liczby pr�bek poszczeg�lnych klas w zbiorze ds
% ds - zbi�r danych do redukcji; pierwsza kolumna zawiera etykiet�
% parts - wierszowy wektor wsp�czynnik�w redukcji dla poszczeg�lnych klas
%NIEZANE UZYCIE NAPISAC
	labels = unique(ds(:,1));
	if rows(labels) ~= columns(parts)
		error("Liczba klas nie zgadza sie z liczba wsp. redukcji.");
	end

	if max(parts) > 1 || min(parts) < 0
		error("Niewlasciwe wspolczynniki redukcji.");
	end
	
	rds=[];
	for i=1:rows(labels)
	    class_samp = ds(ds(:,1) == labels(i), 1:end);
	    N= rows(class_samp);
	    class_samp = class_samp(randperm(N,ceil(N*parts(i))),1:end);
	    rds = [ rds; class_samp ];
	end
	
	% zdecydowanie wypadaloby uzyc randperm do mieszania probek w klasach
	% ta implementacja jest daleka od doskonalosci
	
