function pdf = pdf_parzen(pts, para)
% Aproksymuje warto�� g�sto�ci prawdopodobie�stwa z wykorzystaniem okna Parzena
% pts zawiera punkty, dla kt�rych liczy si� f-cj� g�sto�ci (punkt = wiersz)
% para - struktura zawieraj�ca parametry:
%	para.samples - tablica kom�rek zawieraj�ca pr�bki z poszczeg�lnych klas
%	para.parzenw - szeroko�� okna Parzena
% pdf - macierz g�sto�ci prawdopodobie�stwa
%	liczba wierszy = liczba pr�bek w pts
%	liczba kolumn = liczba klas
	 
	 
  cl_siz = rows(para.samples);
  
  pdf = zeros(rows(pts), cl_siz);
  for cl=1:cl_siz
       samp = para.samples{cl};
    for i_pts=1:rows(pts)
	
     

      hn = para.parzenw/sqrt(rows(samp));
      
      reppts = repmat(pts(i_pts,:), rows(samp), 1);
      u = (reppts-samp(:,:))/hn;
      p_cech  = prod(1/hn * e .^ (-(u.^2)/2) / sqrt(2*pi), 2);	  
      
      pdf(i_pts, cl) = mean(p_cech);
    end
  end

  
  
 
