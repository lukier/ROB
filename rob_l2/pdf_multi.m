function pdf = pdf_multi(pts, para)
% Liczy funkcj� g�sto�ci prawdopodobie�stwa wielowymiarowego r. normalnego
% pts zawiera punkty, dla kt�rych liczy si� f-cj� g�sto�ci (punkt = wiersz)
% para - struktura zawieraj�ca parametry:
%	para.mu - warto�ci �rednie cech (wiersz na klas�)
%	para.sig - macierze kowariancji cech (warstwa na klas�)
% pdf - macierz g�sto�ci prawdopodobie�stwa
%	liczba wierszy = liczba pr�bek w pts
%	liczba kolumn = liczba klas
  d = columns(pts)
  pdf = zeros(rows(pts),d);
  for i = 1:rows(pts)
    for cl = 1:rows(para.mu) % dla kazdej klas
      e_up = -((pts(i,:)-para.mu(cl,:))*inv(para.sig(:,:,cl))*(pts(i,:)-para.mu(cl,:))')/2;
      down = sqrt((2*pi)^(d)*det(para.sig(:,:,cl)));
      px = 1/down*exp(e_up);
      pdf(i,cl)=px; 
    end
  end
end
